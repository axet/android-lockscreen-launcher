package com.github.axet.lockscreenlauncher;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.widgets.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.widgets.PathMax;
import com.github.axet.androidlibrary.widgets.ThemeUtils;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LauncherService extends Service implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String TAG = LauncherService.class.getSimpleName();

    public static final int NOTIFICATION_TORRENT_ICON = 1;

    public static String UPDATE_NOTIFY = LauncherService.class.getCanonicalName() + ".UPDATE_NOTIFY";
    public static String SHOW_ACTIVITY = LauncherService.class.getCanonicalName() + ".SHOW_ACTIVITY";
    public static String APP_CLICK = LauncherService.class.getCanonicalName() + ".APP_CLICK";

    static int[] LAYOUTS_IDS = new int[]{R.layout.notification_big, R.layout.notification_small};
    static int[][] ITEMS_IDS = new int[][]{
            new int[]{R.id.item1, R.id.item_icon1, R.id.item_text1},
            new int[]{R.id.item2, R.id.item_icon2, R.id.item_text2},
            new int[]{R.id.item3, R.id.item_icon3, R.id.item_text3},
            new int[]{R.id.item4, R.id.item_icon4, R.id.item_text4},
            new int[]{R.id.item5, R.id.item_icon5, R.id.item_text5},
            new int[]{R.id.item6, R.id.item_icon6, R.id.item_text6},
            new int[]{R.id.item7, R.id.item_icon7, R.id.item_text7},
            new int[]{R.id.item8, R.id.item_icon8, R.id.item_text8},
            new int[]{R.id.item9, R.id.item_icon9, R.id.item_text9},
            new int[]{R.id.item10, R.id.item_icon10, R.id.item_text10},
            new int[]{R.id.item11, R.id.item_icon11, R.id.item_text11},
    };

    ArrayList<String> list = new ArrayList<>();
    OptimizationPreferenceCompat.ServiceReceiver optimization;
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String a = intent.getAction();
            if (a.equals(Intent.ACTION_USER_PRESENT)) {
                showNotificationAlarm(false);
            }
            if (a.equals(Intent.ACTION_SCREEN_OFF)) {
                showNotificationAlarm(true);
            }
            if (a.equals(Intent.ACTION_SCREEN_ON)) {
                ;
            }
        }
    };

    public static class AppInfo {
        public ComponentName name;
        public ActivityInfo a;
        public Bitmap bm;
        public String s;
        public ApplicationInfo app;
        public PackageInfo p;

        public AppInfo(Context context, String name) {
            this.name = ComponentName.unflattenFromString(name);
            final PackageManager pm = context.getPackageManager();
            Intent intent = new Intent();
            intent.setComponent(this.name);
            ResolveInfo info = pm.resolveActivity(intent, 0);
            Drawable d = info.loadIcon(pm);
            this.bm = drawableToBitmap(d);
            this.a = info.activityInfo;
            this.app = a.applicationInfo;
            try {
                this.p = pm.getPackageInfo(a.packageName, PackageManager.GET_ACTIVITIES);
            } catch (PackageManager.NameNotFoundException e) {
            }

            this.s = a.loadLabel(pm).toString();
            if (this.s == null || this.s.isEmpty()) {
                this.s = FilenameUtils.getExtension(name);
                int i = this.s.indexOf('$');
                if (i != -1)
                    this.s = this.s.substring(i + 1);
            }
        }
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static void startIfEnabled(Context context) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        if (!shared.getBoolean(MainApplication.PREFERENCE_ENABLED, false))
            return;
        SharedPreferences.Editor edit = shared.edit();
        edit.putBoolean(MainApplication.PREFERENCE_RUN, true);
        edit.commit();
        Intent i = new Intent(context, LauncherService.class).setAction(UPDATE_NOTIFY);
        context.startService(i);
    }

    public static void stopService(Context context) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = shared.edit();
        edit.putBoolean(MainApplication.PREFERENCE_RUN, false);
        edit.commit();
        context.stopService(new Intent(context, LauncherService.class));
    }

    public static int fillView(Context context, View v, ArrayList<String> list) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);

        for (int i = 0; i < ITEMS_IDS.length; i++) {
            int item = ITEMS_IDS[i][0];
            int icon = ITEMS_IDS[i][1];
            int text = ITEMS_IDS[i][2];
            View itemView = v.findViewById(item);
            ImageView iconView = (ImageView) v.findViewById(icon);
            TextView textView = (TextView) v.findViewById(text);
            if (itemView != null && iconView != null && textView != null) {
                itemView.setVisibility(View.GONE);
            }
        }

        int i = 0;
        for (; i < list.size() && i < ITEMS_IDS.length; i++) {
            AppInfo info = new AppInfo(context, list.get(i));
            int item = ITEMS_IDS[i][0];
            int icon = ITEMS_IDS[i][1];
            int text = ITEMS_IDS[i][2];
            View itemView = v.findViewById(item);
            ImageView iconView = (ImageView) v.findViewById(icon);
            TextView textView = (TextView) v.findViewById(text);
            if (iconView == null || textView == null)
                break;
            TextView tt = new TextView(context);
            tt.setTextSize(textView.getTextSize() / context.getResources().getDisplayMetrics().scaledDensity);
            tt.setText(info.s);
            PathMax p = new PathMax(context, tt);
            ViewGroup.LayoutParams lp = iconView.getLayoutParams();
            p.measure(lp.width, lp.height);
            iconView.setImageBitmap(info.bm);
            textView.setText(tt.getText());
            itemView.setVisibility(View.VISIBLE);
            int w = View.MeasureSpec.makeMeasureSpec(metrics.widthPixels * 2, View.MeasureSpec.AT_MOST);
            int h = View.MeasureSpec.makeMeasureSpec(metrics.heightPixels * 2, View.MeasureSpec.AT_MOST);
            v.measure(w, h);
            if (v.getMeasuredWidth() > metrics.widthPixels) {
                break; // hide last
            }
        }
        int k = i;
        for (; i < ITEMS_IDS.length; i++) {
            int item = ITEMS_IDS[i][0];
            int icon = ITEMS_IDS[i][1];
            int text = ITEMS_IDS[i][2];
            View itemView = v.findViewById(item);
            ImageView iconView = (ImageView) v.findViewById(icon);
            TextView textView = (TextView) v.findViewById(text);
            if (iconView != null && textView != null) {
                itemView.setVisibility(View.GONE);
            }
        }
        return k;
    }

    public static View generateView(Context context, ViewGroup parent, ArrayList<String> list) {
        LayoutInflater inflater = LayoutInflater.from(context);
        int max = Integer.MIN_VALUE;
        View v = null;
        for (int id : LAYOUTS_IDS) {
            View vv = inflater.inflate(id, parent, false);
            vv.setId(id);
            int k = fillView(context, vv, list);
            if (k > max) {
                max = k;
                v = vv;
            }
        }
        return v;
    }

    public LauncherService() {
    }

    boolean isRunning() {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        return shared.getBoolean(MainApplication.PREFERENCE_RUN, false);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");

        OptimizationPreferenceCompat.REFRESH = AlarmManager.MIN5;
        optimization = new OptimizationPreferenceCompat.ServiceReceiver(this, getClass()) {
            @Override
            public void check() {
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_USER_PRESENT);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(receiver, filter);

        list = MainApplication.load(this);

        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        shared.registerOnSharedPreferenceChangeListener(this);

        if (!isRunning()) {
            stopSelf();
            return;
        }
    }

    MainApplication getApp() {
        return ((MainApplication) getApplication());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");

        if (!isRunning()) {
            stopSelf();
            return START_NOT_STICKY;
        }

        if (optimization.onStartCommand(intent, flags, startId)) {
            Log.d(TAG, "onStartCommand restart");
            showNotificationAlarm(true);
        }

        if (intent != null) {
            String a = intent.getAction();
            if (a != null) {
                if (a.equals(APP_CLICK)) {
                    ComponentName name = ComponentName.unflattenFromString(intent.getStringExtra("app"));
                    Intent s = new Intent();
                    s.setComponent(name);
                    s.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    try {
                        getApp().startActivity(s);
                    } catch (Exception e) {
                        Log.d(TAG, "unable to start", e);
                    }
                }
                if (a.equals(UPDATE_NOTIFY)) {
                    showNotificationAlarm(true);
                }
                if (a.equals(SHOW_ACTIVITY)) {
                    MainActivity.startActivity(this);
                }
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public class Binder extends android.os.Binder {
        public LauncherService getService() {
            return LauncherService.this;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestory");

        if (optimization != null) {
            optimization.close();
            optimization = null;
        }

        stopForeground(false);

        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        shared.unregisterOnSharedPreferenceChangeListener(this);

        showNotificationAlarm(false);

        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
    }

    Notification buildNotification() {
        PendingIntent main = PendingIntent.getService(this, 0,
                new Intent(this, LauncherService.class).setAction(SHOW_ACTIVITY),
                PendingIntent.FLAG_UPDATE_CURRENT);

        FrameLayout parent = new FrameLayout(this);
        View v = generateView(this, parent, list);

        RemoteViews view = new RemoteViews(getPackageName(), v.getId());

        view.setOnClickPendingIntent(R.id.status_bar_latest_event_content, main);

        int i = 0;
        for (; i < list.size() && i < ITEMS_IDS.length; i++) {
            int item = ITEMS_IDS[i][0];
            int icon = ITEMS_IDS[i][1];
            int text = ITEMS_IDS[i][2];
            View itemView = v.findViewById(item);
            ImageView iconView = (ImageView) v.findViewById(icon);
            TextView textView = (TextView) v.findViewById(text);
            if (itemView == null || iconView == null || textView == null)
                break;
            Drawable d = iconView.getDrawable();
            Bitmap bm = drawableToBitmap(d);
            view.setImageViewBitmap(iconView.getId(), bm);
            view.setTextViewText(textView.getId(), textView.getText().toString());
            view.setViewVisibility(itemView.getId(), itemView.getVisibility());

            PendingIntent pe = PendingIntent.getService(this, 1 + i,
                    new Intent(this, LauncherService.class).setAction(APP_CLICK).putExtra("app", list.get(i)),
                    PendingIntent.FLAG_UPDATE_CURRENT);
            view.setOnClickPendingIntent(itemView.getId(), pe);
        }
        for (; i < ITEMS_IDS.length; i++) {
            int item = ITEMS_IDS[i][0];
            int icon = ITEMS_IDS[i][1];
            int text = ITEMS_IDS[i][2];
            view.setViewVisibility(item, View.GONE);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setContentTitle(getString(R.string.app_name))
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentText(getString(R.string.app_name))
                .setContent(view);

        if (Build.VERSION.SDK_INT >= 21)
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        return builder.build();
    }

    public void showNotificationAlarm(boolean show) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (!show) {
            notificationManager.cancel(NOTIFICATION_TORRENT_ICON);
        } else {
            notificationManager.notify(NOTIFICATION_TORRENT_ICON, buildNotification());
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.d(TAG, "onTaskRemoved");
        optimization.onTaskRemoved(rootIntent);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        list = MainApplication.load(this);
        showNotificationAlarm(true);
    }
}

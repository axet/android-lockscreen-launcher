package com.github.axet.lockscreenlauncher;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.commons.io.FilenameUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class MainActivity extends AppCompatActivity {

    ArrayList<String> list = new ArrayList<>();
    ApplicationsAdapter apps;
    Button neutral;
    AlertDialog d;
    FrameLayout frame;

    public static String componentName(String p, String a) {
        return new ComponentName(p, a).flattenToShortString();
    }

    public class Package {
        public boolean expand; // expaned?
        public ApplicationInfo app;
        public PackageInfo pack;
        public ActivityInfo activity;
        public String name;

        public Package(ApplicationInfo a, PackageInfo p, ActivityInfo aa) {
            this.app = a;
            this.pack = p;
            this.activity = aa;
            this.name = componentName(app.packageName, activity.name);
        }

        public String getPackageName() {
            return app.packageName;
        }

        public boolean isChecked() {
            for (ActivityInfo i : pack.activities) {
                if (list.contains(componentName(i.packageName, i.name)))
                    return true;
            }
            return false;
        }

        public Drawable loadIcon() {
            final PackageManager pm = getPackageManager();
            return app.loadIcon(pm);
        }
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    public class ApplicationsAdapter extends BaseAdapter {
        List<Package> packages;

        public ApplicationsAdapter() {
            final PackageManager pm = getPackageManager();
            // packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
            Set<String> ss = new TreeSet<>();
            packages = new ArrayList<>();
            Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            List<ResolveInfo> pkgAppsList = pm.queryIntentActivities(mainIntent, 0);
            if (pkgAppsList == null)
                return; // Android Studio editor
            for (ResolveInfo info : pkgAppsList) {
                ApplicationInfo i = info.activityInfo.applicationInfo;
                if (!ss.contains(i.packageName)) {
                    ss.add(i.packageName);
                    try {
                        PackageInfo p = pm.getPackageInfo(i.packageName, PackageManager.GET_ACTIVITIES);
                        packages.add(new Package(i, p, info.activityInfo));
                    } catch (PackageManager.NameNotFoundException e) {
                        ;
                    }
                }
            }
        }

        @Override
        public int getCount() {
            return packages.size();
        }

        @Override
        public Object getItem(int position) {
            return packages.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                convertView = inflater.inflate(R.layout.application_item, parent, false);
            }
            final View v = convertView;
            final Package info = packages.get(position);
            ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
            TextView text = (TextView) convertView.findViewById(R.id.item_text);
            TextView sum = (TextView) convertView.findViewById(R.id.item_summary);
            final CheckBox sw = (CheckBox) convertView.findViewById(R.id.item_switch);

            String n = MainApplication.getApplicationName(MainActivity.this, info.app);
            Drawable d = info.loadIcon();

            icon.setImageDrawable(d);
            text.setText(n);
            sum.setText(info.getPackageName());
            sw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vv) {
                    if (list.contains(info.name))
                        list.remove(info.name);
                    else
                        list.add(info.name);
                    updateList(v, info);
                    updateNotification();
                    save();
                }
            });
            sw.setChecked(info.isChecked());
            final View item_more = convertView.findViewById(R.id.item_more);
            item_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vv) {
                    info.expand = !info.expand;
                    updateList(v, info);
                }
            });
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item_more.performClick();
                }
            });
            updateList(convertView, info);
            return convertView;
        }

        void updateList(View convertView, Package info) {
            final ActivitiesAdapter a = new ActivitiesAdapter(info);
            LinearLayout list = (LinearLayout) convertView.findViewById(R.id.listview);
            int i = 0;
            if (info.expand) {
                for (; i < a.getCount(); i++) {
                    View v = list.getChildAt(i);
                    View vv = a.getView(i, v, list);
                    if (v == null)
                        list.addView(vv);
                }
            }
            for (; i < list.getChildCount(); ) {
                list.removeViewAt(i);
            }
        }
    }

    public class ActivitiesAdapter extends BaseAdapter {
        Package p;
        ArrayList<ActivityInfo> aa = new ArrayList<>();

        public ActivitiesAdapter(Package p) {
            this.p = p;
            loadAll();
        }

        void loadAll() {
            for (ActivityInfo info : p.pack.activities)
                aa.add(info);
        }

        void loadLaunchers() {
            PackageManager pm = getPackageManager();
            Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            List<ResolveInfo> appList = pm.queryIntentActivities(mainIntent, 0);
            for (ResolveInfo info : appList) {
                ApplicationInfo i = info.activityInfo.applicationInfo;
                if (i.packageName.equals(p.app.packageName)) {
                    aa.add(info.activityInfo);
                }
            }
        }

        @Override
        public int getCount() {
            return aa.size();
        }

        @Override
        public ActivityInfo getItem(int position) {
            return aa.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                convertView = inflater.inflate(R.layout.application_item_activity, parent, false);
            }
            final ActivityInfo info = getItem(position);
            final String n = componentName(info.packageName, info.name);
            PackageManager pm = getPackageManager();
            ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
            TextView text = (TextView) convertView.findViewById(R.id.item_text);
            TextView sum = (TextView) convertView.findViewById(R.id.item_summary);
            final CheckBox sw = (CheckBox) convertView.findViewById(R.id.item_switch);

            Drawable d = p.app.loadIcon(pm);
            icon.setImageDrawable(d);

            text.setText(FilenameUtils.getExtension(info.name));
            sum.setText(info.name);
            sw.setOnCheckedChangeListener(null);
            sw.setChecked(list.contains(n));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sw.performClick();
                }
            });
            sw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (list.contains(n))
                        list.remove(n);
                    else
                        list.add(n);
                    apps.notifyDataSetChanged();
                    updateNotification();
                    save();
                }
            });
            return convertView;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView list = (ListView) findViewById(R.id.listview);

        this.list = MainApplication.load(this);

        apps = new ApplicationsAdapter();
        list.setAdapter(apps);

        frame = (FrameLayout) findViewById(R.id.notifications);

        updateNotification();

        LauncherService.startIfEnabled(this);
    }

    public void save() {
        MainApplication.save(this, list);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.enable) {
            final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
            boolean b = !item.isChecked();
            SharedPreferences.Editor edit = shared.edit();
            edit.putBoolean(MainApplication.PREFERENCE_ENABLED, b);
            edit.commit();
            item.setChecked(b);
            LauncherService.startIfEnabled(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void updateNotification() {
        View v = LauncherService.generateView(this, frame, list);
        frame.removeAllViews();
        frame.addView(v);
        v.requestLayout();
    }
}

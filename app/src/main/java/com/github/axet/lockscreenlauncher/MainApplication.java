package com.github.axet.lockscreenlauncher;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.preference.PreferenceManager;

import org.apache.commons.io.FilenameUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class MainApplication extends Application {

    public static final String PREFERENCE_RUN = "run";
    public static final String PREFERENCE_ENABLED = "enabled";

    public static final String PREFERENCE_APP_PREFIX = "app_";
    public static final String PREFERENCE_APP_COUNT = "count";

    public static void save(Context context, ArrayList<String> set) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = shared.edit();
        edit.putInt(PREFERENCE_APP_PREFIX + PREFERENCE_APP_COUNT, set.size());
        for (int i = 0; i < set.size(); i++) {
            edit.putString(PREFERENCE_APP_PREFIX + i, set.get(i));
        }
        edit.commit();
    }

    public static ArrayList<String> load(Context context) {
        ArrayList<String> set = new ArrayList<>();
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        int count = shared.getInt(PREFERENCE_APP_PREFIX + PREFERENCE_APP_COUNT, 0);
        for (int i = 0; i < count; i++) {
            String s = shared.getString(PREFERENCE_APP_PREFIX + i, "");
            ComponentName name = ComponentName.unflattenFromString(s);
            set.add(name.flattenToShortString());
        }
        return set;
    }

    public static String toString(Object o) {
        if (o == null)
            return null;
        return o.toString();
    }

    public static String getApplicationName(Context context, String pkg) {
        final PackageManager pm = context.getPackageManager();
        try {
            ApplicationInfo info;
            info = pm.getApplicationInfo(pkg, 0);
            return getApplicationName(context, info);
        } catch (final PackageManager.NameNotFoundException e) {
            return pkg;
        }
    }

    public static String getApplicationName(Context context, ApplicationInfo info) {
        final PackageManager pm = context.getPackageManager();
        String n = toString(pm.getApplicationLabel(info));
        if (n == null || n.isEmpty())
            n = toString(info.loadLabel(pm));
        if (n == null || n.isEmpty())
            n = info.packageName;
        return n;
    }
}
